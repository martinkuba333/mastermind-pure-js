starting();

const MAX_DIFF = 10;
const MIN_DIFF = 3;
const MAX_ROWS = 10;
const COLOR = ["green", "lightblue", "pink", "black", "brown", "yellow", "red", "orange", "gray", "violet"];

const NOTHING = "nothing";
const SOMETHING = "something";
const EXACTLY = "exactly";

let difficulty = 5;
let final = [];
let possibilities = [];
let clickedColor = "";
let unlockedCircle = 0;
let currentLine = 0;

function starting() {
  let startButton = document.getElementsByClassName("start-button")[0];
  startButton.addEventListener("click", () => {
    toggleStartingHide();
    prepare();
  });
}

function toggleStartingHide() {
  let start = document.getElementsByClassName("start")[0];
  start.classList.toggle("hide");

  let boardTipping = document.getElementsByClassName("board-tipping")[0];
  boardTipping.classList.toggle("hide");
}

function prepare() {
  setDifficulty();
  fillFinal();
  fillFinalBoard();
  fillChoosingBoard();
  fillTippingBoard();
  playerLogic();
}

function playerLogic() {
  colorChooseEvents();
  colorTipEvents();
  checkLine();
}

function checkLine() {
  let boardTipping = document.getElementsByClassName("board-tipping-set")[0];
  boardTipping.addEventListener("click", () => {
    if (canCloseActiveLine()) {
      let answer = processAnswer();
      answer = rearangeAnswer(answer);
      showAnswers(answer);
      unlockNextLine();
      let endResponse = checkEndGame(answer);
      if (endResponse.length > 0) {
        endGame(endResponse);
      } else {
        colorTipEvents();
      }
    }
  });
}

function rearangeAnswer(answer) {
  let result = [];
  let countExact = 0;
  let countSomething = 0;

  for (let i = 0; i < answer.length; i++) {
    if (answer[i] === EXACTLY) {
      countExact++;
    }
    if (answer[i] === SOMETHING) {
      countSomething++;
    }
  }

  for (let i = 0; i < answer.length; i++) {
    if (i < countExact) {
      result.push(EXACTLY);
    } else if (i < countExact + countSomething) {
      result.push(SOMETHING);
    } else {
      result.push(NOTHING);
    }
  }

  return result;
}

function endGame(response) {
  setTimeout(() => {
    alert("YOU " + response.toUpperCase() + " THE GAME!!");
  }, 100);
}

function showAnswers(answers) {
  let answerCircle = document.querySelectorAll(".active .board-tipping-block-circle-answer");
  for (let i = 0; i < answers.length; i++) {
    answerCircle[i].classList.add(answers[i]);
  }
}

function processAnswer() {
  let tippedColors = getTippedColors();
  let tempFinal = final.slice(0);
  let answer = [];
  for (let i = 0; i < tempFinal.length; i++) {
    if (tempFinal[i] === tippedColors[i]) {
      answer.push(EXACTLY);
      tempFinal[i] = "";
    }
  }

  for (let i = 0; i < tippedColors.length; i++) {
    let tempFinalIndex = tempFinal.indexOf(tippedColors[i]);
    if (tempFinalIndex > -1) {
      answer.push(SOMETHING);
      tempFinal[tempFinalIndex] = "";
    }
  }

  while (answer.length < final.length) {
    answer.push(NOTHING);
  }

  return answer;
}

function getTippedColors() {
  let tippedColors = [];
  let tippingCircles = document.querySelectorAll(".active .board-tipping-block-circle.set");
  tippingCircles.forEach(circle => {
    circle.classList.forEach(nameClass => {
      if (COLOR.indexOf(nameClass) > -1) {
        tippedColors.push(nameClass);
      }
    });
  });
  return tippedColors;
}

function canCloseActiveLine() {
  let activeCirclesSet = document.querySelectorAll(".active .set");
  return activeCirclesSet.length === parseInt(difficulty);
}

function unlockNextLine() {
  unlockedCircle = parseInt(unlockedCircle) + parseInt(difficulty);
  let boardTippingBlockCircle = document.querySelectorAll(".board-tipping-block-circle");

  for (let i = 0; i < boardTippingBlockCircle.length; i++) {
    boardTippingBlockCircle[i].removeEventListener("click", setColor);
  }
  let boardTippingBlock = document.querySelectorAll(".board-tipping-block");
  boardTippingBlock[currentLine].classList.remove("active");
  boardTippingBlock[currentLine].classList.add("passive");
  currentLine++;
}

function checkEndGame(answer) {
  let result = "";
  if ((new Set(answer)).size === 1 && answer[0] === EXACTLY) {
    showFinal();
    result = "win";
  } else if (difficulty * MAX_ROWS <= unlockedCircle) {
    showFinal();
    result = "lose";
  }
  return result;
}

function showFinal() {
  let boardFinalCircles = document.getElementsByClassName("board-final-circle");
  for (let i = 0; i < boardFinalCircles.length; i++) {
    boardFinalCircles[i].classList.add(final[i]);
  }
}

function colorTipEvents() {
  let boardTippingBlockCircle = document.querySelectorAll(".board-tipping-block-circle");

  let size = parseInt(difficulty) + parseInt(unlockedCircle);
  for (let i = unlockedCircle; i < size; i++) {
    boardTippingBlockCircle[i].addEventListener("click", setColor);
  }

  let boardTippingBlock = document.querySelectorAll(".board-tipping-block");
  boardTippingBlock[currentLine].classList.add("active");
}

function setColor(e) {
  let circle = e.target;
  if (clickedColor.length > 0) {
    COLOR.forEach(c => {
      circle.classList.remove(c);
    });
    circle.classList.add(clickedColor);
    circle.classList.add("set");
  }
}

function colorChooseEvents() {
  let boardChoosingCircle = document.getElementsByClassName("board-choosing-circle");
  for (let i = 0; i < boardChoosingCircle.length; i++) {
    boardChoosingCircle[i].addEventListener("click", chosenColor);
  }
}

function chosenColor() {
  let classes = this.classList;
  for (let i = 0; i < classes.length; i++) {
    if (COLOR.indexOf(classes[i]) !== -1) {
      clickedColor = classes[i];
    }
  }

  changeText();
}

function changeText() {
  let boardChoosingText = document.getElementsByClassName("board-choosing-text")[0];
  boardChoosingText.classList = "";
  boardChoosingText.classList.add(clickedColor);
  boardChoosingText.classList.add("board-choosing-text");
  boardChoosingText.innerText = clickedColor;
}

function fillTippingBoard() {
  let boardTipping = document.getElementsByClassName("board-tipping")[0];

  fillTippingCircles(boardTipping);
}

function fillTippingCircles(element) {
  for (let j = 0; j < MAX_ROWS; j++) {
    let boardTippingBlock = document.createElement("div");
    boardTippingBlock.classList.add("board-tipping-block");
    for (let i = 0; i < difficulty; i++) {
      createCircle(boardTippingBlock, "board-tipping-block-circle");
    }
    for (let i = 0; i < difficulty; i++) {
      createCircle(boardTippingBlock, "board-tipping-block-circle-answer");
    }
    element.append(boardTippingBlock);
  }
}

function fillChoosingBoard() {
  let boardChoosing = document.getElementsByClassName("board-choosing")[0];

  for (let i = 0; i < difficulty; i++) {
    createCircle(boardChoosing, "board-choosing-circle", possibilities[i]);
  }
}

function fillFinalBoard() {
  let boardFinal = document.getElementsByClassName("board-final")[0];
  for (let i = 0; i < difficulty; i++) {
    createCircle(boardFinal, "board-final-circle");
  }
}

function createCircle(element, className, otherClassName = "") {
  let div = document.createElement("div");
  div.classList.add("circle");
  div.classList.add(className);
  if (otherClassName !== "") {
    div.classList.add(otherClassName);
  }
  element.append(div);
}

function setDifficulty() {
  let startDifficulty = document.getElementsByClassName("start-difficulty")[0];
  difficulty = startDifficulty.value;
  if (difficulty < MIN_DIFF) difficulty = MIN_DIFF;
  if (difficulty > MAX_DIFF) difficulty = MAX_DIFF;
}

function fillFinal() {
  for (let i = 0; i < difficulty; i++) {
    possibilities.push(COLOR[i]);
  }
  for (let i = 0; i < difficulty; i++) {
    let rand = Math.floor(Math.random() * difficulty);
    final.push(possibilities[rand]);
  }
}
